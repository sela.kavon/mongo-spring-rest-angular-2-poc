package toptal.sample.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import toptal.sample.domain.TimeEntry;

import java.util.Collection;
import java.util.Date;

public interface TimeEntryRepository extends MongoRepository<TimeEntry, String> {

    Collection<TimeEntry> findByUserName(String userName);

    Collection<TimeEntry> findByUserNameAndDateBetween(
            String userName, Integer dateFrom, Integer dateTo);

    void deleteByUserName(String userName);
}
