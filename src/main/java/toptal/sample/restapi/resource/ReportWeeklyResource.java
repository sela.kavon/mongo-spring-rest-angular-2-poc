package toptal.sample.restapi.resource;

import org.springframework.hateoas.ResourceSupport;
import toptal.sample.domain.report.ReportWeek;
import toptal.sample.restapi.controller.ReportWeekRestController;
import toptal.sample.restapi.resource.report.ReportWeekResource;

import java.util.Collection;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by ala on 12.5.16.
 */
public class ReportWeeklyResource extends ResourceSupport {

    private Collection<ReportWeekResource> reportWeeks;

    public ReportWeeklyResource(Collection<ReportWeek> reportWeeks, String userName) {

        this.reportWeeks = reportWeeks.stream()
                .map(
                        reportWeek -> new ReportWeekResource(reportWeek, userName)
                ).collect(Collectors.toList());

        add(linkTo(
                methodOn(ReportWeekRestController.class, userName).getWeeklyReport(userName)
        ).withSelfRel());
    }

    public Collection<ReportWeekResource> getReportWeeks() {
        return reportWeeks;
    }
}
