package toptal.sample.restapi.resource;

import org.springframework.hateoas.ResourceSupport;
import toptal.sample.domain.User;
import toptal.sample.restapi.controller.ReportWeekRestController;
import toptal.sample.restapi.controller.TimeEntryRestController;
import toptal.sample.restapi.controller.UserRestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by ala on 9.5.16.
 */
public class UserResource extends ResourceSupport {

    private String fullName;

    private String userName;

    private String roles[];

    public UserResource(User user) {

        this.fullName = user.getFullName();

        this.userName = user.getUserName();

        this.roles = user.getRoles();

        this.add(linkTo(methodOn(
                UserRestController.class
                ).getUser(user.getUserName())
        ).withSelfRel());

        this.add(linkTo(methodOn(
                TimeEntryRestController.class, user.getUserName()
        ).getTimeEntries(user.getUserName(), null, null)
        ).withRel("timeEntries"));

        this.add(linkTo(methodOn(
                ReportWeekRestController.class, user.getUserName()
                ).getWeeklyReport(user.getUserName())
        ).withRel("reportWeeks"));
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }
}
